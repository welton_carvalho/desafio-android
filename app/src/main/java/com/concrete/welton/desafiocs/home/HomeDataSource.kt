package com.concrete.welton.desafiocs.home

import android.arch.paging.DataSource
import android.arch.paging.TiledDataSource
import com.concrete.welton.desafiocs.ApiConfig
import com.concrete.welton.desafiocs.PageRepository
import com.concrete.welton.desafiocs.Repository
import com.concrete.welton.desafiocs.RepositoryService
import retrofit2.Response

/**
 */
class HomeDataSource : TiledDataSource<Repository>() {

    var service: RepositoryService = ApiConfig.instance.service()

    override fun loadRange(startPosition: Int, count: Int): List<Repository> {
        val response: Response<PageRepository> = service.loadRepositories(startPosition, count).execute()
        return response.body()?.respository ?: listOf()
    }

    override fun countItems(): Int {
        return DataSource.COUNT_UNDEFINED
    }
}