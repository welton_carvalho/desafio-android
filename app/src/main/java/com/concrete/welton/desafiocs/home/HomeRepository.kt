package com.concrete.welton.desafiocs.home

import android.arch.lifecycle.MutableLiveData
import com.concrete.welton.desafiocs.*


/**
 */
class HomeRepository {

    var service: RepositoryService = ApiConfig.instance.service()

    fun loadRepositories(): MutableLiveData<Resource<MutableList<Repository>>> {
        val liveData = MutableLiveData<Resource<MutableList<Repository>>>()

//        service.loadRepositories(0,10).enqueue(object : Callback<PageRepository> {
//            override fun onResponse(call: Call<PageRepository>?, response: Response<PageRepository>?) {
//                liveData.value = Resource.success(response?.body()?.respository)
//            }
//
//            override fun onFailure(call: Call<PageRepository>?, t: Throwable) {
//                liveData.value = Resource.error(AppException(t))
//            }
//
//        })
        return liveData
    }
}