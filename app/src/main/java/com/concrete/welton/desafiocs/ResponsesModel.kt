package com.concrete.welton.desafiocs

import com.google.gson.annotations.SerializedName

/**
 */
data class Repository(val name: String,
                      @SerializedName("full_name") val fullName: String,
                      val description: String,
                      @SerializedName("forks") val forks: String,
                      @SerializedName("stargazers_count") val stars: String,
                      val owner: Owner)

data class Owner(val login: String,
                 @SerializedName("avatar_url") val avatar: String)

data class PageRepository(@SerializedName("items") val respository: List<Repository>)