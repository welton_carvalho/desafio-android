package com.concrete.welton.desafiocs.home

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListProvider
import android.arch.paging.PagedList
import com.concrete.welton.desafiocs.Repository


/**
 */

class HomeViewModel : ViewModel() {

    var repositories: LiveData<PagedList<Repository>>
    lateinit var dataSource: HomeDataSource

    init {
        repositories = object : LivePagedListProvider<Int, Repository>() {
            override fun createDataSource(): DataSource<Int, Repository> {
                dataSource = HomeDataSource()
                return dataSource
            }

        }.create(0, PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(20)
                .setInitialLoadSizeHint(20)
                .build())
    }


}
