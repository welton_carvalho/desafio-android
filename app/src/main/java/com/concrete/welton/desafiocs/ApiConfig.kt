package com.concrete.welton.desafiocs

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 */
class ApiConfig private constructor() {
    private val repositoryService: RepositoryService

    private object Holder {
        val INSTANCE = ApiConfig()
    }

    companion object {
        val instance: ApiConfig by lazy { Holder.INSTANCE }
    }

    init {

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
        val retrofit = Retrofit.Builder()
                .baseUrl("http://api.github.com/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        repositoryService = retrofit.create(RepositoryService::class.java)
    }

    fun service(): RepositoryService = repositoryService
}