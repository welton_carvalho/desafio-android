package com.concrete.welton.desafiocs

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 */
interface RepositoryService {

    @GET("search/repositories?q=language:Java&sort=stars")
    fun loadRepositories(@Query("page") since: Int, @Query("per_page") perPage: Int): Call<PageRepository>
}