package com.concrete.welton.desafiocs.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import com.concrete.welton.desafiocs.R
import com.concrete.welton.desafiocs.ViewHelper.find


class HomeActivity : AppCompatActivity() {

    internal lateinit var homeViewModel: HomeViewModel
    internal lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository)
        recyclerView = find(this, R.id.repository_recycler_view)
        recyclerView.setHasFixedSize(true)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val adapter = HomeAdapter()
        recyclerView.adapter = adapter

        homeViewModel.repositories.observe(this, Observer { pagedList -> adapter.setList(pagedList) })

    }
}
