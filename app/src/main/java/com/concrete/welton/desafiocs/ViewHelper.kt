package com.concrete.welton.desafiocs

import android.app.Activity
import android.support.annotation.IdRes
import android.view.View

/**
 */

object ViewHelper {

    fun <T : View> find(activity: Activity, @IdRes id: Int): T {
        return activity.findViewById(id)
    }

    fun <T : View> find(view: View, @IdRes id: Int): T {
        return view.findViewById(id)
    }
}
