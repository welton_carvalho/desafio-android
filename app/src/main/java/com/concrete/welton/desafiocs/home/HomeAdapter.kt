package com.concrete.welton.desafiocs.home

import android.arch.paging.PagedListAdapter
import android.graphics.drawable.BitmapDrawable
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.recyclerview.extensions.DiffCallback
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.concrete.welton.desafiocs.R
import com.concrete.welton.desafiocs.Repository
import com.concrete.welton.desafiocs.ViewHelper.find
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso


/**
 */

class HomeAdapter : PagedListAdapter<Repository, HomeAdapter.ViewHolder>(DIFF_CALLBACK) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.repository_item_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!)

    }


    class ViewHolder(val v: View) : RecyclerView.ViewHolder(v) {
        private var name: TextView
        private var description: TextView
        private var forksCount: TextView
        private var starsCount: TextView
        private var owner: TextView
        private var avatar: ImageView


        init {
            name = find(v, R.id.repository_name)
            description = find(v, R.id.repository_description)
            forksCount = find(v, R.id.repository_forks_count)
            starsCount = find(v, R.id.repository_stars_count)
            owner = find(v, R.id.repository_owner)
            avatar = find(v, R.id.repository_avatar)
        }

        fun bind(repository: Repository) {
            name.text = repository.name
            description.text = repository.description
            forksCount.text = repository.forks
            starsCount.text = repository.stars
            owner.text = repository.owner.login

            Picasso.with(v.context).load(repository.owner.avatar)
                    .placeholder(R.drawable.avatar_placeholder)
                    .into(avatar, object : Callback {
                        override fun onSuccess() {
                            val imageBitmap = (avatar.getDrawable() as BitmapDrawable).bitmap
                            val imageDrawable = RoundedBitmapDrawableFactory.create(v.resources, imageBitmap)
                            imageDrawable.isCircular = true
                            imageDrawable.cornerRadius = Math.max(imageBitmap.width, imageBitmap.height) / 2.0f
                            avatar.setImageDrawable(imageDrawable)
                        }

                        override fun onError() {
                            avatar.setImageResource(R.drawable.avatar_placeholder)
                        }

                    })
        }
    }


    object DIFF_CALLBACK : DiffCallback<Repository>() {
        override fun areContentsTheSame(oldItem: Repository, newItem: Repository): Boolean {
            return oldItem.equals(newItem)
        }

        override fun areItemsTheSame(oldItem: Repository, newItem: Repository): Boolean {
            return oldItem.name == newItem.name
        }

    }
}
